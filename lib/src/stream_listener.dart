import 'dart:async';

import 'package:flutter/widgets.dart';

///
/// A widget that allows you to listen a stream within the widget tree without rebuilding its child:
/// ```dart
/// class MyView extends StatelessWidget {
///   final userActionsController = StreamController<MyEvent>();
///
///   const MyView({super.key});
///
///   void _onUserAction(UserAction action) {
///     print('New user action: $action');
///   }
///
///   void _onShowModal(BuildContext context, MyState state) {
///     showDialog(context: context, (context) => Text('New state: $state'));
///   }
///
///   @override
///   Widget build(BuildContext context) {
///     // Here we suppose that a MyViewModel instance has been injected with the package: flutter_inject
///     final logic = Dependency.get<MyViewModel>(context);
///     // Listen one or multiple streams
///     return StreamListener(
///       listeners: [
///         (context) => userActionsController.stream.listen(_onUserAction),
///         (context) => logic.stream.whereType<ShowModal>().listen((e) => _onShowModal(context, e)),
///       ],
///       child: Scaffold(
///         floatingActionButton: FloatingActionButton(
///           onPressed: () => userActionsController.add(UserAction()),
///           child: const Icon(Icons.add_rounded),
///         ),
///       ),
///     );
///   }
/// }
/// ```
///
class StreamListener extends StatefulWidget {
  /// A list of methods that return a [StreamSubscription] for a given [BuildContext]
  final List<StreamSubscription Function(BuildContext)> listeners;

  /// The lucky child widget that won't be rebuilt when listened streams will trigger events
  final Widget child;

  /// The constructor of our [StreamListener] widget
  const StreamListener({super.key, required this.listeners, required this.child});

  /// The [State] factory of this [StatefulWidget]
  @override
  State<StreamListener> createState() => _StreamListenerState();
}

/// The [State] class of our [StatefulWidget] widget
class _StreamListenerState extends State<StreamListener> {
  /// The stream subscriptions that gonna be disposed with this [StatefulWidget]
  final subscriptions = <StreamSubscription>[];

  /// The initState method that init the [subscriptions] property
  @override
  void initState() {
    super.initState();
    subscriptions.addAll(widget.listeners.map((e) => e(context)));
  }

  /// The dispose method called when this [StatefulWidget] will be removed from the widget tree
  @override
  void dispose() {
    for (final subscription in subscriptions) {
      subscription.cancel();
    }
    super.dispose();
  }

  /// The build method which will just return the given child
  @override
  Widget build(BuildContext context) {
    return widget.child;
  }
}
