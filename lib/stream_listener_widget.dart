/// A simple widget that listen streams without rebuilding its child (ex. to show a dialog)
library stream_listener_widget;

export 'src/stream_listener.dart';
