# stream_listener_widget_example

The classic counter example to show how it's easy to use the stream_listener_widget package.

Here we use stream_listener_widget to show a dialog when the counter reached or exceeded 5.

## main.dart

```
    import 'dart:async';
    import 'package:flutter/material.dart';
    import 'package:stream_listener_widget/stream_listener_widget.dart';
    
    void main() => runApp(const MaterialApp(home: CounterView()));
    
    class CounterView extends StatefulWidget {
      const CounterView({super.key});
    
      @override
      State<CounterView> createState() => _CounterViewState();
    }
    
    class _CounterViewState extends State<CounterView> {
      final logic = CounterLogic();
    
      void _onMaxReached(int state) {
        showDialog(
          context: context,
          barrierDismissible: true,
          builder: (context) {
            return Dialog(
              child: Padding(
                padding: const EdgeInsets.all(20),
                child: Text('Your counter: $state has reached or exceeded max'),
              ),
            );
          },
        );
      }
    
      @override
      void dispose() {
        logic.dispose();
        super.dispose();
      }
    
      @override
      Widget build(BuildContext context) {
        return StreamListener(
          listeners: [
            (context) => logic.maxReached.listen(_onMaxReached),
          ],
          child: Scaffold(
            appBar: AppBar(title: const Text('Counter demo')),
            body: Center(
              child: StreamBuilder(
                initialData: logic.state,
                stream: logic.controller.stream,
                builder: (context, snapshot) => Text('Counter: ${snapshot.data}'),
              ),
            ),
            floatingActionButton: FloatingActionButton(
              onPressed: logic.increment,
              child: const Icon(Icons.add),
            ),
          ),
        );
      }
    }
    
    class CounterLogic {
      int state = 0;
      final controller = StreamController<int>.broadcast();
    
      Stream<int> get maxReached => controller.stream.where((e) => e >= 5);
    
      void increment() => controller.add(++state);
    
      void dispose() => controller.close();
    }
```