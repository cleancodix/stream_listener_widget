# stream_listener_widget_example

The classic counter example to show how it's easy to use the stream_listener_widget package.

Here we use stream_listener_widget to show a dialog when the counter reached or exceeded 5.