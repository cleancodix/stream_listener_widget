import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:stream_listener_widget_example/main.dart';

void main() {
  group('Counter test', () {
    testWidgets('Original Flutter counter test', (WidgetTester tester) async {
      // Build our app and trigger a frame
      await tester.pumpWidget(const MaterialApp(home: CounterView()));

      // Verify that our counter starts at 0
      expect(find.text('Counter: 0'), findsOneWidget);
      expect(find.text('Counter: 1'), findsNothing);

      // Tap the '+' icon and trigger a frame
      await tester.tap(find.byIcon(Icons.add));
      await tester.pump();

      // Verify that our counter has incremented
      expect(find.text('Counter: 0'), findsNothing);
      expect(find.text('Counter: 1'), findsOneWidget);
    });

    testWidgets('Show dialog when max reached test', (WidgetTester tester) async {
      // Build our app and trigger a frame
      await tester.pumpWidget(const MaterialApp(home: CounterView()));

      // Verify that our counter starts at 0
      expect(find.text('Counter: 0'), findsOneWidget);

      // Tap the '+' icon 5 times and trigger a frame
      for (var i = 0; i < 5; i++) {
        await tester.tap(find.byIcon(Icons.add));
      }
      await tester.pump();

      // Verify that our counter has incremented
      expect(find.text('Counter: 0'), findsNothing);
      expect(find.text('Counter: 5'), findsOneWidget);

      // Verify that max dialog is displayed
      final dialog = find.text('Your counter: 5 has reached or exceeded max');
      expect(dialog, findsOneWidget);

      // Tap outside of the dialog and trigger a frame
      await tester.tap(find.text('Counter demo'), warnIfMissed: false);
      // Note: warnIfMissed is set to false to avoid a warning
      // because the text "Counter demo" is behind the Dialog's barrier and so it's not directly tapped
      await tester.pump();

      // Verify that max dialog is displayed
      expect(dialog, findsNothing);
    });
  });
}
